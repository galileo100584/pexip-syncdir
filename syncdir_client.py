#!/usr/bin/python
import os
import sys
import time
import socket

def watcher(file_list):
    look_for_deleted_files(file_list)

    for dirName, subdirList, fileList in os.walk(working_dir):
        for fname in fileList:
            fullname = dirName + "/" + fname
            relativepath = os.path.relpath(fullname)
            lastmodified = os.path.getmtime(relativepath)
            row=0.0	#init value

            try:
                row = file_list[relativepath]
            except KeyError, e:
			    print " "#'I got a KeyError - reason "%s"' % str(e)
            
            if lastmodified > row:
                transfer(relativepath)
                file_list[relativepath]=lastmodified
            
def remove_remote_file(relativepath):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    print("REMOVE FILE (" + relativepath + ") ON SERVER......")
    header_str = "DEL " + relativepath
    s.send(header_str)
    s.close()


def transfer(relativepath):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    print("TRANSFER FILE (" + relativepath + ") TO SERVER......")

    header_str = "PUT " + relativepath
    s.send(header_str)
    s.close()

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))

    f = open(relativepath, 'rb')
    partial = f.read(1024)
    while partial:
        s.send(partial)
        partial = f.read(1024)
    f.close()

    s.close()


def init_system(file_list):

    for dirName, subdirList, fileList in os.walk(working_dir):
        for fname in fileList:
            fullname = dirName + "/" + fname
            relativepath = os.path.relpath(fullname)
            lastmodified = os.path.getmtime(relativepath)
            transfer(relativepath)
            file_list[relativepath]=lastmodified

def look_for_deleted_files(file_list):
    remove_list=[]
    for relativepath in file_list:
        if not os.path.isfile(relativepath):
            remove_remote_file(relativepath)
            remove_list.append(relativepath)

    for x in remove_list:
        del file_list[x]

	
if __name__ == "__main__":
    os.stat_float_times(True)

    global_update_list={}	   

    port = 10000
    server = sys.argv[1]
    backup_path = sys.argv[2]

    os.chdir(backup_path)
    working_dir = os.getcwd()

    host = server

    print("server " + server + "\n")
    print("path " + working_dir + "\n\n")

    init_system(global_update_list)
    print("\nEntering watch mode\n")

    while(1):
        time.sleep(5)
        watcher(global_update_list)

    sys.exit(0)
