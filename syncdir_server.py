#!/usr/bin/python
import os, sys, time
import socket
import shutil

def listen():
    while True:
        conn, addr = s.accept()
        header = conn.recv(1024)	#i.e. (1024 byes)

        print header

        cmd = header[:3]
        filename = header[4:]

        if  cmd == "PUT":
            putfile(filename)

        elif cmd == "DEL":
            delfile(filename)
            #remove file

def delfile(filename):
    if os.path.isfile(filename):
        os.unlink(filename)

def putfile(filename):
    conn, addr = s.accept()
    
    full_path = working_dir + "/" + filename
    full_dir = os.path.dirname(full_path)
    if not os.path.exists(full_dir):
        os.makedirs(full_dir)
        
    with open(full_path, 'wb+') as f:
        while True:
            #recive file
           
            data = conn.recv(1024)
           
            if not data:
                break
            f.write(data)
        f.close

def prepare_dir(storage_dir):
    for the_file in os.listdir(storage_dir):
        file_path = os.path.join(storage_dir, the_file)
        try:
            if os.path.isfile(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path): shutil.rmtree(file_path)
        except Exception as e:
            print(e)

if __name__ == "__main__":

    storage_dir = sys.argv[2]
    host = sys.argv[1]

    if not os.path.exists(storage_dir):
        os.makedirs(storage_dir)

    os.chdir(storage_dir)
    working_dir = os.getcwd()

    #empty out dir
    prepare_dir(working_dir)

    port = 10000
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    
    s.bind((host, port))
    s.listen(1) # only allow 1 connection   

    print "Server ready"
    listen()
